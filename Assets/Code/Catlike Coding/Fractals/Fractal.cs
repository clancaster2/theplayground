﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fractal : MonoBehaviour
{
	public Mesh[] meshes;
	public Material material;

	public int maxDepth;
	private int depth;

	public float minChildScale;
	public float maxChildScale;
	private float childScale;
	public float spawnProbability;
	public float maxRotationSpeed;
	private float rotationSpeed;
	public float maxTwist;

	private static Vector3[] childDirections =
	{
		Vector3.up,
		Vector3.right,
		Vector3.left,
		Vector3.forward,
		Vector3.back,
	};

	private static Quaternion[] childOrientations =
	{
		Quaternion.identity,
		Quaternion.Euler(0f, 0f, -90f),
		Quaternion.Euler(0f, 0f, 90f),
		Quaternion.Euler(90f, 0f, 0f),
		Quaternion.Euler(-90, 0f, 0f),
	};

	private Material[,] materials;

	// Use this for initialization
	void Start()
	{
		if (materials == null)
		{
			InitializeMaterials();
		}
		gameObject.AddComponent<MeshFilter>().mesh = meshes[Random.Range(0, meshes.Length)];
		gameObject.AddComponent<MeshRenderer>().material = materials[depth, Random.Range(0, 3)];
		rotationSpeed = Random.Range(-maxRotationSpeed, maxRotationSpeed);
		transform.Rotate(Random.Range(-maxTwist, maxTwist), 0f, 0f);
		if (depth < maxDepth)
		{
			StartCoroutine(CreateChildren());
		}
	}

	// Update is called once per frame
	void Update()
	{
		transform.Rotate(0f, rotationSpeed * Time.deltaTime, 0f);
	}

	private void Initialize(Fractal parent, int childIndex)
	{
		meshes = parent.meshes;
		materials = parent.materials;
		maxDepth = parent.maxDepth;
		depth = parent.depth + 1;
		minChildScale = parent.minChildScale;
		maxChildScale = parent.maxChildScale;
		spawnProbability = parent.spawnProbability;
		maxRotationSpeed = parent.maxRotationSpeed;
		maxTwist = parent.maxTwist;
		transform.parent = parent.transform;
		childScale = Random.Range(minChildScale, maxChildScale);
		transform.localScale = Vector3.one * childScale;
		transform.localPosition = childDirections[childIndex] * (0.5f + 0.5f * childScale);
		transform.localRotation = childOrientations[childIndex];
	}

	private void InitializeMaterials()
	{
		materials = new Material[maxDepth + 1, 3];
		for (int i = 0; i <= maxDepth; i++)
		{
			float t = i / (maxDepth - 1f);
			t *= t;
			materials[i, 0] = new Material(material);
			materials[i, 0].color = material.color = Color.Lerp(Color.white, Color.yellow, (float) i / maxDepth);
			materials[i, 1] = new Material(material);
			materials[i, 1].color = material.color = Color.Lerp(Color.white, Color.red, (float)i / maxDepth);
			materials[i, 2] = new Material(material);
			materials[i, 2].color = material.color = Color.Lerp(Color.black, Color.blue, (float)i / maxDepth);
		}
		materials[maxDepth, 0].color = Color.cyan;
		materials[maxDepth, 1].color = Color.magenta;
		materials[maxDepth, 2].color = Color.white;
	}

	private IEnumerator CreateChildren()
	{
		for (int i = 0; i < childDirections.Length; i++)
		{
			if (Random.value < spawnProbability)
			{
				yield return new WaitForSeconds(Random.Range(0.2f, 0.6f));
				new GameObject("Fractal Child").
						AddComponent<Fractal>().Initialize(this, i);
			}
		}
	}
}
