﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtifactManager : MonoBehaviour
{
    //Summary
    //This script will manage the artifact objects, which are the players main goal
    //Managing includes spawning, tracking etc

    public int artifactStartAmount;
    public GameObject[] artifacts;
    public Transform[] artifactSpawnLocations;
    public List<Transform> artifactUsedSpawns;

    int artifactsCollected;
    int artifacctsRemaining;

    // Use this for initialization
    void Start()
    {
        artifactStartAmount = artifacts.Length;
        SpawnArtifacts();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SpawnArtifacts()
    {
        for (int a = 0; a < artifactStartAmount; a++)
        {
            int heighestWeight = 0;
            int currentSpawn = 0;
            //Loops through the possible spawns, ignoring any already used
            //Chooses which spawn to use by assigning random spawn weights while looping and choosing the spawn with the highest weight
            for (int i = 0; i < artifactSpawnLocations.Length; i++)
            {
                //Only runs for empty spawns
                if (!artifactUsedSpawns.Contains(artifactSpawnLocations[i]))
                {
                    int spawnWeight = Random.Range(1, 100);
                    //This point has the new heighest weight and should be substituted into the current spawn variable
                    if (spawnWeight > heighestWeight)
                    {
                        currentSpawn = i;
                        heighestWeight = spawnWeight;
                    }
                }
            }
            //Once spawn is decided, spawn artifact, and add spawn location to used list, so the location cannot be used for a second artifact
            Debug.Log(currentSpawn + " " + heighestWeight);
            artifactUsedSpawns.Add(artifactSpawnLocations[currentSpawn]);
            Spawn(a, currentSpawn);
        }
    }

    void Spawn(int artifactNumber, int artifactLocation)
    {
        //Spawns the given artifact at the given transform
        GameObject artifact = Instantiate(artifacts[artifactNumber], artifactSpawnLocations[artifactLocation].position, artifactSpawnLocations[artifactLocation].rotation);
        artifact.GetComponent<Artifact>().artifactManager = this;
    }

    void ArtifactCollected()
    {
        artifactsCollected += 1;
        artifactsCollected -= 1;
        //Launch increase difficulty function here
    }
}