﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu]
public class ShapeFactory : ScriptableObject
{
	Scene poolScene;

	[SerializeField]
	Shape[] prefabs;

	[SerializeField]
	Material[] materials;

	[SerializeField]
	bool recycle;

	List<Shape>[] pools;

	void CreatePools()
	{
		pools = new List<Shape>[prefabs.Length];
		for (int i = 0; i < pools.Length; i++)
		{
			pools[i] = new List<Shape>();
		}
		if (Application.isEditor)
		{
			poolScene = SceneManager.GetSceneByName(name);
			if (poolScene.isLoaded)
			{
				GameObject[] rootObjects = poolScene.GetRootGameObjects();
				for (int i = 0; i < rootObjects.Length; i++)
				{
					Shape pooledShape = rootObjects[i].GetComponent<Shape>();
					if (!pooledShape.gameObject.activeSelf)
					{
						pools[pooledShape.ShapeID].Add(pooledShape);
					}
				}
				return;
			}
		}
		poolScene = SceneManager.CreateScene(name);
	}

	public Shape Get(int shapeID = 0, int materialID = 0)
	{
		Shape instance;
		if (recycle)
		{
			if (pools == null)
			{
				CreatePools();
			}
			List<Shape> pool = pools[shapeID];
			int lastIndex = pool.Count - 1;
			if (lastIndex >= 0)
			{
				instance = pool[lastIndex];
				instance.gameObject.SetActive(true);
				pool.RemoveAt(lastIndex);
			}
			else
			{
				instance = Instantiate(prefabs[shapeID]);
				instance.OriginFactory = this;
				instance.ShapeID = shapeID;
				SceneManager.MoveGameObjectToScene(instance.gameObject, poolScene);
			}
		}
		else
		{
			instance = Instantiate(prefabs[shapeID]);
			instance.ShapeID = shapeID;
		}
		instance.SetMaterial(materials[materialID], materialID);
		return instance;
	}

	public Shape GetRandom()
	{
		return Get(Random.Range(0, prefabs.Length), Random.Range(0, materials.Length));
	}

	public void Reclaim(Shape shapeToRecycle)
	{
		if (shapeToRecycle.OriginFactory != this)
		{
			Debug.LogError("Tried to reclaim shape with wrong factory");
			return;
		}
		if (recycle)
		{
			if (pools == null)
			{
				CreatePools();
			}
			pools[shapeToRecycle.ShapeID].Add(shapeToRecycle);
			shapeToRecycle.gameObject.SetActive(false);
		}
		else
		{
			Destroy(shapeToRecycle.gameObject);
		}
	}

	public int FactoryId
	{
		get
		{
			return factoryId;
		}
		set
		{
			if (factoryId == int.MinValue && value != int.MinValue)
			{
				factoryId = value;
			}
			else
			{
				Debug.Log("Not allowed to change factoryId");
			}
		}
	}

	[System.NonSerialized]
	int factoryId = int.MinValue;
}
