﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableItem : MonoBehaviour
{
    //Summary
    //This is a class for items that the player can interact with
    //Will likely serve as a parent class for the primary items the player will be collecting, but may also serve as a parent for other classes,-
    //such as notes or optional lore items the player can read, if they are implemented 

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public virtual void Interact()
    {
        Debug.Log("Interacted with " + gameObject.name);
        gameObject.SetActive(false);
    }
}
