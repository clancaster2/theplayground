﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour
{
    bool flashlightOn;
    public Light thisLight;

    // Use this for initialization
    void Start()
    {
        flashlightOn = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            ToggleLight();
        }
    }

    void ToggleLight()
    {
        //Toggles the light component and updates the bool tracking its current on/off status
        thisLight.enabled = !flashlightOn;
        flashlightOn = !flashlightOn;
    }

    void Flicker()
    {

    }
}
