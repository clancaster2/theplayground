﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexCell : MonoBehaviour
{
    public HexCoordinates coordinates;

    Color color;

    int elevation = int.MinValue;

    [SerializeField]
    HexCell[] neighbours;

    bool hasIncomingRiver, hasOutgoingRiver;
    HexDirections incomingRiver, outgoingRiver;

    [SerializeField]
    bool[] roads;

    public RectTransform uiRect;

    public HexGridChunk chunk;

    public HexCell GetNeighbour(HexDirections direction)
    {
        return neighbours[(int)direction];
    }

    public void SetNeighbour(HexDirections direction, HexCell cell)
    {
        neighbours[(int)direction] = cell;
        cell.neighbours[(int)direction.Opposite()] = this;
    }

    public bool HasIncomingRiver
    {
        get
        {
            return hasIncomingRiver;
        }
    }

    public bool HasOutgoingRiver
    {
        get
        {
            return hasOutgoingRiver;
        }
    }

    public HexDirections IncomingRiver
    {
        get
        {
            return incomingRiver;
        }
    }

    public HexDirections OutgoingRiver
    {
        get
        {
            return outgoingRiver;
        }
    }

    public bool HasRiver
    {
        get
        {
            return hasIncomingRiver || hasOutgoingRiver;
        }
    }

    public bool HasRiverBeginOrEnd
    {
        get
        {
            return hasIncomingRiver != hasOutgoingRiver;
        }
    }

    public bool HasRiverThroughEdge(HexDirections direction)
    {
        return hasIncomingRiver && incomingRiver == direction ||
            hasOutgoingRiver && outgoingRiver == direction;
    }

    public void SetOutgoingRiver(HexDirections direction)
    {
        if (HasOutgoingRiver && outgoingRiver == direction)
        {
            return;
        }

        HexCell neighbour = GetNeighbour(direction);
        if (!neighbour || elevation < neighbour.elevation)
        {
            return;
        }

        RemoveOutgoingRiver();
        if (hasIncomingRiver && incomingRiver == direction)
        {
            RemoveIncomingRiver();
        }
        hasOutgoingRiver = true;
        outgoingRiver = direction;

        neighbour.RemoveIncomingRiver();
        neighbour.hasIncomingRiver = true;
        neighbour.incomingRiver = direction.Opposite();

        SetRoad((int)direction, false);
    }

    public void RemoveRiver()
    {
        RemoveOutgoingRiver();
        RemoveIncomingRiver();
    }

    public void RemoveOutgoingRiver()
    {
        if (!hasOutgoingRiver)
        {
            return;
        }
        hasOutgoingRiver = false;
        RefreshSelfOnly();

        HexCell neighbour = GetNeighbour(outgoingRiver);
        neighbour.hasIncomingRiver = false;
        neighbour.RefreshSelfOnly();
    }

    public void RemoveIncomingRiver()
    {
        if (!hasIncomingRiver)
        {
            return;
        }
        hasIncomingRiver = false;
        RefreshSelfOnly();

        HexCell neighbour = GetNeighbour(incomingRiver);
        neighbour.hasOutgoingRiver = false;
        neighbour.RefreshSelfOnly();
    }

    public HexDirections RiverBeginOrEndDirection
    {
        get
        {
            return hasIncomingRiver ? incomingRiver : outgoingRiver;
        }
    }

    public float StreamBedY
    {
        get
        {
            return 
                (elevation + HexMetrics.streamBedElevationOffset) * 
                HexMetrics.elevationStep;
        }
    }

    public float RiverSurfaceY
    {
        get
        {
            return
                (elevation + HexMetrics.riverSurfaceElevationOffset) * HexMetrics.elevationStep;
        }
    }

    public bool HasRoads
    {
        get
        {
            for (int i = 0; i < roads.Length; i++)
            {
                if (roads[i])
                {
                    return true;
                }
            }
            return false;
        }
    }

    public bool HasRoadThroughEdge (HexDirections direction)
    {
        return roads[(int)direction];
    }

    public void AddRoad (HexDirections direction)
    {
        if (!roads[(int)direction] && !HasRiverThroughEdge(direction) && 
            GetElevationDifference(direction) <=1)
        {
            SetRoad((int)direction, true);
        }
    }

    public void RemoveRoads()
    {
        for (int i = 0; i < neighbours.Length; i++)
        {
            if (roads[i])
            {
                SetRoad(i, false);
            }
        }
    }

    void SetRoad (int index, bool state)
    {
        roads[index] = state;
        neighbours[index].roads[(int)((HexDirections)index).Opposite()] = state;
        neighbours[index].RefreshSelfOnly();
        RefreshSelfOnly();
    }

    public int Elevation
    {
        get
        {
            return elevation;
        }
        set
        {
            if (elevation == value)
            {
                return;
            }
            elevation = value;
            Vector3 position = transform.localPosition;
            position.y = value * HexMetrics.elevationStep;
            position.y +=
                (HexMetrics.SampleNoise(position).y * 2f - 1f) *
                HexMetrics.elevationPerturbStrength;
            transform.localPosition = position;

            Vector3 uiPoisiton = uiRect.localPosition;
            uiPoisiton.z = -position.y;
            uiRect.localPosition = uiPoisiton;

            if (
                hasOutgoingRiver &&
                elevation < GetNeighbour(outgoingRiver).elevation
                )
            {
                RemoveOutgoingRiver();
            }
            if(
                hasIncomingRiver &&
                elevation > GetNeighbour(incomingRiver).elevation
                )
            {
                RemoveIncomingRiver();
            }

            for (int i = 0; i < roads.Length; i++)
            {
                if (roads[i] && GetElevationDifference((HexDirections)i) > 1)
                {
                    SetRoad(i, false);
                }
            }

            Refresh();
        }
    }

    public int GetElevationDifference (HexDirections direction)
    {
        int difference = elevation - GetNeighbour(direction).elevation;
        return difference >= 0 ? difference : -difference;
    }

    public Color Color
    {
        get
        {
            return color;
        }
        set
        {
            if (color == value)
            {
                return;
            }
            color = value;
            Refresh();
        }
    }

    public HexEdgeType GetEdgeType(HexDirections direction)
    {
        return HexMetrics.GetEdgeType(
            elevation, neighbours[(int)direction].elevation
        );
    }

    public HexEdgeType GetEdgeType(HexCell otherCell)
    {
        return HexMetrics.GetEdgeType(elevation, otherCell.elevation);
    }

    public Vector3 Position
    {
        get
        {
            return transform.localPosition;
        }
    }

    void Refresh()
    {
        if (chunk)
        {
            chunk.Refresh();
            for (int i = 0; i < neighbours.Length; i++)
            {
                HexCell neighbour = neighbours[i];
                if (neighbour != null && neighbour.chunk != chunk)
                {
                    neighbour.chunk.Refresh();
                }
            }
        }
    }

    void RefreshSelfOnly()
    {
        chunk.Refresh();
    }
}
