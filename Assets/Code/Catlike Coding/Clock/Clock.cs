﻿using System;
using UnityEngine;

public class Clock : MonoBehaviour
{
    public Transform hoursTransform, minutesTransform, secondsTransform;
    const float degreesPerHour = 30.0f;
    const float degreesPerMinute = 6.0f;
    const float degreesPerSecond = 6.0f;

    public bool continuous;

    void Awake()
    {
       
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (continuous)
        {
            UpdateContinuous();
        }
        else
        {
            UpdateDiscrete();
        }
    }

    void UpdateContinuous()
    {
        TimeSpan time = DateTime.Now.TimeOfDay;
        hoursTransform.localRotation =
            Quaternion.Euler(0, (float) time.TotalHours * degreesPerHour, 0);
        minutesTransform.localRotation =
            Quaternion.Euler(0, (float) time.TotalMinutes * degreesPerMinute, 0);
        secondsTransform.localRotation =
            Quaternion.Euler(0, (float) time.TotalSeconds * degreesPerSecond, 0);
    }

    void UpdateDiscrete()
    {
        DateTime time = DateTime.Now;
        hoursTransform.localRotation =
            Quaternion.Euler(0, time.Hour * degreesPerHour, 0);
        minutesTransform.localRotation =
            Quaternion.Euler(0, time.Minute * degreesPerMinute, 0);
        secondsTransform.localRotation =
            Quaternion.Euler(0, time.Second * degreesPerSecond, 0);
    }
}
