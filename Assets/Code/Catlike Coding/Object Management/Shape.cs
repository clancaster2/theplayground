﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape : PersistableObject
{
	public Vector3 angularVelocity { get; set; }
	public Vector3 Velocity { get; set; }

	public int ShapeID
	{
		get { return shapeID; }
		set
		{
			if (shapeID == int.MinValue && value != int.MinValue)
			{
				shapeID = value;
			}
			else
			{
				Debug.LogError("Not allowed to change shapeId");
			}
		}
	}
	int shapeID = int.MinValue;

	public ShapeFactory OriginFactory
	{
		get
		{
			return originFactory;
		}
		set
		{
			if (originFactory == null)
			{
				originFactory = value;
			}
			else
			{
				Debug.LogError("Not allowed to change origin factory");
			}
		}
	}

	ShapeFactory originFactory;

	public int MaterialId { get; private set; }

	[SerializeField]
	MeshRenderer[] meshRenderers;

	public void SetMaterial(Material material, int materialID)
	{
		for (int i = 0; i < meshRenderers.Length; i++)
		{
			meshRenderers[i].material = material;
		}
		MaterialId = materialID;
	}

	Color[] colors;

	public int ColorCount
	{
		get
		{
			return colors.Length;
		}
	}

	private void Awake()
	{
		colors = new Color[meshRenderers.Length];
	}

	static int colorPropertyID = Shader.PropertyToID("_Color");

	static MaterialPropertyBlock sharedPropertyBlock;

	public void SetColor(Color color)
	{
		if (sharedPropertyBlock == null)
		{
			sharedPropertyBlock = new MaterialPropertyBlock();
		}
		sharedPropertyBlock.SetColor(colorPropertyID, color);
		for (int i = 0; i < meshRenderers.Length; i++)
		{
			colors[i] = color;
			meshRenderers[i].SetPropertyBlock(sharedPropertyBlock);
		}
	}

	public void SetColor(Color color, int index)
	{
		if (sharedPropertyBlock == null)
		{
			sharedPropertyBlock = new MaterialPropertyBlock();
		}
		sharedPropertyBlock.SetColor(colorPropertyID, color);
		colors[index] = color;
		meshRenderers[index].SetPropertyBlock(sharedPropertyBlock);
	}

	public override void Save(GameDataWriter writer)
	{
		base.Save(writer);
		writer.Write(colors.Length);
		for (int i = 0; i < colors.Length; i++)
		{
			writer.Write(colors[i]);
		}
		writer.Write(angularVelocity);
		writer.Write(Velocity);
	}

	public override void Load(GameDataReader reader)
	{
		base.Load(reader);
		if (reader.Version >= 5)
		{
			LoadColors(reader);
		}
		else
		{
			SetColor(reader.Version > 0 ? reader.ReadColor() : Color.white);
		}
		angularVelocity = reader.Version >= 4 ? reader.ReadVector3() : Vector3.zero;
		Velocity = reader.Version >= 4 ? reader.ReadVector3() : Vector3.zero;
	}

	void LoadColors(GameDataReader reader)
	{
		int count = reader.ReadInt();
		int max = count <= colors.Length ? count : colors.Length;
		int i = 0;
		for (; i < max; i++)
		{
			SetColor(reader.ReadColor(), i);
		}
		if (count > max)
		{
			for (; i < count; i++)
			{
				reader.ReadColor();
			}
		}
		else if (count < max)
		{
			for (; i < max; i++)
			{
				SetColor(Color.white, i);
			}
		}
	}

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	public void GameUpdate()
	{
		transform.Rotate(angularVelocity * Time.deltaTime);
		transform.localPosition += Velocity * Time.deltaTime;
	}

	public void Recycle()
	{
		OriginFactory.Reclaim(this);
	}
}
