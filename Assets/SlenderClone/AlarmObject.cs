﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlarmObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        //BUG - getting all players colliders, finding parent, ergo running more than once
        if (other.GetComponentInParent<PlayerController>() != null)
        {
            Debug.Log("Sound the alarm");
        }
    }
}
