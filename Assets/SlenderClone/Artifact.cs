﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Artifact : InteractableItem
{
    public ArtifactManager artifactManager;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Interact()
    {
        base.Interact();
        //Announce collection
        //TODO - Turn this into an event call, rather than hardcoded connections later on 

    }
}
