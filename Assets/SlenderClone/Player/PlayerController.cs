﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    float walkSpeed, sprintSpeed, strafeSpeed, jumpForce, rotationSpeed;

    float playerSpeed;

    [SerializeField]
    Rigidbody rb;

    [SerializeField]
    Camera mainCam;

    float minCamRot = -60f;
    float maxCamRot = 60f;

    float rotY = 0f;

    bool isGrounded;

    float playerHeight = 1f;

    // Use this for initialization
    void Start()
    {
        playerSpeed = walkSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        //Checks a raycast down to determine if player is grounded
        if (Physics.Raycast(transform.position, -Vector3.up, playerHeight + 0.1f))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
        //Various Input checks and function calls
        //TODO - Refactor hard coded keys to variables for binding
        //Sprint control check
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            playerSpeed = sprintSpeed;
        }
        else
        {
            playerSpeed = walkSpeed;
        }
        //Movement (WASD) control check
        //BUG - is grounded check not working with strafe, maybe some if statement bodmas shenanigans.
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0 && isGrounded)
        {
            HandleMovement(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        }
        //Mouse movement check
        if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
        {
            HandleLookRotation(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        }
        //Jump control check
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            Jump();
        }
        //Interact control check
        if (Input.GetKeyDown(KeyCode.E))
        {
            AttemptInteraction();
        }
    }

    void HandleMovement(float horizontal, float vertical)
    {
        //TODO - implement time.deltatime to create more stable functionality
        //Converts the players input variables to movement vectors
        Vector3 movementForce = new Vector3(horizontal * strafeSpeed, 0, vertical * playerSpeed);
        //Ensures the movement vector is in local space
        movementForce = rb.transform.TransformDirection(movementForce);
        //Applies the movement vector
        rb.velocity = (movementForce); //* playerSpeed);
    }

    void HandleLookRotation(float yaw, float pitch)
    {
        //Debug.Log(yaw + " " + pitch);
        //Stores the current rotation values in a modifiable variable
        //Vector3 camRotation = mainCam.transform.rotation.eulerAngles;
        Vector3 bodyRotation = rb.transform.rotation.eulerAngles;
        //Modifies and clamps the pitch of the camera
        //float pitchModification = pitch * rotationSpeed * Time.deltaTime;
        rotY += pitch * rotationSpeed;
        rotY = Mathf.Clamp(rotY, minCamRot, maxCamRot);
        //camRotation.x += pitchModification;
        //camRotation.x = Mathf.Clamp(camRotation.x, minCamRot, maxCamRot);
        //camRotation.x = -camRotation.x;
        //Modifies the yaw rotation of the body
        bodyRotation.y += (yaw * rotationSpeed);
        //Applies the modified rotations
        //mainCam.transform.rotation = Quaternion.Euler(camRotation);
        mainCam.transform.localEulerAngles = new Vector3(-rotY, 0, 0);
        rb.transform.rotation = Quaternion.Euler(bodyRotation);
    }

    void Jump()
    {
        //Adds velocity in the upward direction and ensures the player no longer registers as grounded, preventing adding additional jump force with additional funciton calls
        Vector3 currentVel = rb.velocity;
        currentVel.y += jumpForce;
        rb.velocity = currentVel;
        Debug.Log("Jump");
        isGrounded = false;
    }

    void AttemptInteraction()
    {
        //Raycasts into the cameras forward vector, continuing if the item hit has an interactable component
        RaycastHit hit;
        if (Physics.Raycast(transform.position, mainCam.transform.forward, out hit, 3))
        {
            if (hit.collider.gameObject.GetComponent<InteractableItem>() != null)
            {
                hit.collider.gameObject.GetComponent<InteractableItem>().Interact();
            }
        }
    }
}
